## Assumptions

1. API will be public and will not require any kind of authentication
2. There is no limit on number of requests to the API
3. Number 2 word will convert number to word where number is a currency in dollars.


## Working

This API has a few endpoints  and the documentation of the API can be found [here](public/documentation/index.html) or it can be accesed using the link `https://themtest.codingdiver.co.uk/documentation`


## How to Deploy

### Deploy on Server
- Prerequisite: Apache/nginx, php 7.4, modrewrite should be enabled/installed on the server.
- Pull repo project from git provider and put then inside the public directory of your domain.
- cd to project directory and rename `.env.example` file to `.env`inside your project root and SendGrid Creds 
- Open the console and cd your project root directory
- Run `composer install` or ```php composer.phar install```
- Run `php artisan key:generate` 
- Run Migration Scripts 
- Change the root directory of your domain in apache2(assuming you are using apache) to point to public directoy inside your project root folder
- On public server change APP_ENV=Production and APP_DEBUG=False in `.env` file
### Deploy Locally 
- Prerequisite: Apache/nginx, php 7.2, modrewrite should be enabled/inatlled on the server.
- Pull repo project from git provider and put then inside the a folder on your computer.
- cd to project directory and rename `.env.example` file to `.env`inside your project root and SendGrid Creds
- Open the console and cd your project root directory
- Run `composer install` or ```php composer.phar install```
- Run `php artisan key:generate`
- Run Migration Scripts 
- Run `php artisan serve`  


## Tests
Few test cases have been written to test the functionality of the API. These tests can be found under `tests\Feature\apiTest.php`. To run the test please run the Unit test command form the console. Steps for running the test are

- cd to the project root directory from console
- run the command `.\vendor\bin\phpunit` 
