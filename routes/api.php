<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/team', 'Api\Api@TeamAction');

Route::post('/email', 'Api\Api@EmailAction');

Route::get('/viewemail', 'Api\Api@ViewEmailAction');

Route::get('/track', 'Api\Api@PixelTrack');

Route::get('/number2words','Api\Api@Number2WordsAction');

