<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Emails;
use App\Team;
use App\Helpers\Number2WordsHelper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use Mail;
use App\Mail\SendEmail;

class Api extends Controller
{
    const ERROR_CODE = 400;
    const SUCCESS_CODE = 200;
    public function TeamAction(Request $request)
    {

        try {
            if ($request->input('filter') && $request->input('filter') != 'all') {  // If filter parameter is provided then employees will be filtered
                $team = Team::where('dept', $request->input('filter'))->get();
            } else {
                $team = Team::all();
            }

            $response['data'] = $team;
            $statusCode = self::SUCCESS_CODE;
            return response()->json($response, $statusCode);
        } catch (Exception $e) {

            $response['error'] = "Error occured while processing your request";
            $statusCode = self::ERROR_CODE;
            return response()->json($response, $statusCode);
        }
    }

    public function EmailAction(Request $request)
    {

        $rules = array(
            'name'    => 'required|string',
            'email'   => 'required|email',
            'body'    => 'required|string',
            'subject' => 'required|string',
            'body'    => 'required| string'
        );

        $validator = Validator::make($request->all(), $rules); // check request payload validation based on top rules

        if (isset($validator) && $validator->fails()) {

            $response['error'] = $validator->errors();
            $statusCode = self::ERROR_CODE;

            return response()->json($response, $statusCode);
        } else {

            try {
                $emails = new Emails(); // Create Email entity and stores value in database
                $emails->name    = $request->input('name'); // This will get encrypted using functions inside model
                $emails->email   = $request->input('email'); // This will get encrypted using functions inside model
                $emails->subject = $request->input('subject');
                $emails->body    = $request->input('body');
                $unique = md5(uniqid($request->input, true)); // Creates unique if for tracking
                $emails->token    = $unique;

                if ($emails->save()) {
                    $response['data'] = "Thank you! We will get back to you soon";
                    $statusCode = self::SUCCESS_CODE;

                    // Building payload for email.
                    $data = [
                        'id' => $unique,
                        'from'  => "demo@themtest.codingdiver.co.uk",
                        'subject' => $request->input('subject'),
                
                     ];
                    Mail::to($request->input('email'))->send(new SendEmail($data));

                    if (!count(Mail::failures()) > 0) {
                        $emails->status = 1;
                        $emails->save();
                    }

                    return response()->json($response, $statusCode);
                }
            } catch (Exception $e) {

                $response['error'] = $validator->errors();
                $statusCode = self::ERROR_CODE;
                return response()->json($response, $statusCode);
            }
        }
    }

    /**
     * Shows all email received from contact us form.
     */
    public function ViewEmailAction(Request $request)
    {

        try {

            $Emails = Emails::all();
            $response['data'] = $Emails;
            $statusCode = self::SUCCESS_CODE;
            return response()->json($response, $statusCode);
        } catch (Exception $e) {

            $response['error'] = "Error occured while processing your request";
            $statusCode = self::ERROR_CODE;
            return response()->json($response, $statusCode);
        }
    }

    /**
     * Generates a 1x1 px image which along with tracking code is used to calculate the views of email.
     */
    public function PixelTrack(Request $request)
    {

        if (!empty($request->input('track'))) {
            $emails = Emails::where('token', '=', $request->input('track'))->firstOrFail();
            if ($emails) {
                $emails->views = $emails->views + 1;
                $emails->save();
            }
        }

        echo "\x47\x49\x46\x38\x37\x61\x1\x0\x1\x0\x80\x0\x0\xfc\x6a\x6c\x0\x0\x0\x2c\x0\x0\x0\x0\x1\x0\x1\x0\x0\x2\x2\x44\x1\x0\x3b";
    }

    /**
     * Converts number to word assumption is numbers are always currency in dollars
     */

    public function Number2WordsAction(Request $request)
    {


        $rules = array(
            'number' => 'required|numeric',
            'name'   => 'required|string',
        );

        $validator = Validator::make($request->all(), $rules); // check request payload validation based on top rules

        if (isset($validator) && $validator->fails()) {

            $response['error'] = $validator->errors();
            $statusCode = self::ERROR_CODE;

            return response()->json($response, $statusCode);
        } else {
            $number2Words = new Number2WordsHelper();
            try {
                $result = $number2Words->convertNumber2Words($request->input('number'));
                $response['data'] = $request->input('name') . " " . $result;
                $statusCode = self::SUCCESS_CODE;
                return response()->json($response, $statusCode);
            } catch (Exception $e) {

                $response['error'] = $validator->errors();
                $statusCode = self::ERROR_CODE;
                return response()->json($response, $statusCode);
            }
        }
    }
}
