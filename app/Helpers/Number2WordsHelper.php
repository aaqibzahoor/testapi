<?php

namespace App\Helpers;

class Number2WordsHelper
{

  /**
   * This function converts the number to work using PHP number Format class
   */
  public function convertNumber2Words($number)
  {

    $f = new \NumberFormatter(locale_get_default(), \NumberFormatter::SPELLOUT);
    /** If the number is float then we convent the covert the part before the decimal and after the decimal separately
     *  Because if converted all together number is after the decimals are converted as single digits
     *  */  
    if ($this->containsDecimal($number)) {
      $cents = 'cents';
      list($whole, $decimal) = explode('.', $number);
       $f = new \NumberFormatter(locale_get_default(), \NumberFormatter::SPELLOUT);
       $words1 = $f->format($whole);
       $word2 =  $f->format($decimal);
       $response = $words1." dollars and ".$word2.' cents'; // Assuming all conversions are for currency
       return strtoupper($response);
    }
    else{
      $f = new \NumberFormatter(locale_get_default(), \NumberFormatter::SPELLOUT);
      $words = $f->format($number);
      $words = $words. " dollars"; //adding dollars to the end of the word
      return strtoupper($words);
    }
  }

    /**
   * This function checks if the number is integer or float
   */

  public function containsDecimal($value)
  {
    if (strpos($value, ".") !== false) {
      return true;
    }
  }
}
