<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8" />
  </head>
  <body>
    <h2>Thank You</h2>
    <p>We have received your email and will get back to you as soon as possible.</p>
    <img src='https://themtest.codingdiver.co.uk/api/track?track={{ $id }}' />
  </body>
</html>