<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class apiTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_if_endpoint_is_working()
    {
        $response = $this->get('/api/number2words');

        $response->assertStatus(400); // Because i am not passing values end point will return 400
    }


    public function test_numbers_2_words()
    {

        $requertData = [
            'name' => 'John Smith',
            'number' => '200.77',

        ];

        $responseData = [
            'data' => 'John Smith TWO HUNDRED DOLLARS AND SEVENTY-SEVEN CENTS'
        ];

        $response = $this->json('GET', '/api/number2words', $requertData);
        $response->assertStatus(200)
                 ->assertJson($responseData);
    }
}
